package org.duineth.mobile.gps.alarm.android;

import java.io.IOException;
import java.io.StreamCorruptedException;
import java.util.List;

import org.duineth.mobile.gps.alarm.cont.LocationListnerHandler;
import org.duineth.mobile.gps.alarm.cont.SettingsHandler;
import org.duineth.mobile.gps.alarm.res.AlarmData;
import org.duineth.mobile.gps.alarm.res.Observers;

import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;
import android.widget.SeekBar;

public class AlarmService extends Service implements Observers {

	private static final String TAG = AlarmService.class.getName();
	private LocationListnerHandler lcHandler = LocationListnerHandler
			.getInstance();

	private SettingsHandler settingsHandler = null;

	/**
	 * Class for clients to access. Because we know this service always runs in
	 * the same process as its clients, we don't need to deal with IPC.
	 */
	public class LocalBinder extends Binder {
		AlarmService getService() {
			return AlarmService.this;
		}
	}

	public AlarmService() {
	}

	@Override
	public void onCreate() {

		try {
			settingsHandler = SettingsHandler.getInstance(AlarmService.this);
		} catch (StreamCorruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (ClassNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		lcHandler.addObservers(AlarmService.this);
		try {
			lcHandler.startLocation(AlarmService.this);
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		Log.i(TAG, "START ALARM SERVICE");

	}

	@Override
	public void onRebind(Intent intent) {
		try {
			lcHandler.startLocation(AlarmService.this);
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		Log.i(TAG, "START ALARM SERVICE");
	}

	@Override
	public void onStart(Intent intent, int startId) {
		try {
			lcHandler.startLocation(AlarmService.this);
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		Log.i(TAG, "START ALARM SERVICE");
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		Log.i("LocalService", "Received start id " + startId + ": " + intent);
		return START_STICKY;
	}

	@Override
	public void onDestroy() {
		lcHandler.addObservers(AlarmService.this);
	}

	@Override
	public IBinder onBind(Intent intent) {
		return mBinder;
	}

	private final IBinder mBinder = new LocalBinder();

	public void update(Location location) {

		List<AlarmData> alarmDatas = settingsHandler.getAlarmDatas();

		for (AlarmData alarmData : alarmDatas) {

			if (alarmData.isWorking()) {
				double distance = alarmData.getDistance(location);
				if (distance < 10) {

					Uri alert = RingtoneManager
							.getDefaultUri(RingtoneManager.TYPE_ALARM);
					if (alert == null) {
						// alert is null, using backup
						alert = RingtoneManager
								.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
						if (alert == null) { // I can't see this ever being null
												// (as always have a default
												// notification) but just incase
							// alert backup is null, using 2nd backup
							alert = RingtoneManager
									.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
						}
					}

					Ringtone r = RingtoneManager.getRingtone(
							getApplicationContext(), alert);
					r.play();

				}
			}

		}

	}
}
