package org.duineth.mobile.gps.alarm.android;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

public class BootReceiver extends BroadcastReceiver {
	private static final String TAG = BootReceiver.class.getName();

	public BootReceiver() {
	}

	@Override
	public void onReceive(Context context, Intent intent) {
		
		Log.i(TAG, "Start Location Listner");
		
		Intent locationIntent = new Intent(AlarmService.class.getName());
		context.startActivity(locationIntent);
	}
}
