package org.duineth.mobile.gps.alarm.android;

import java.io.IOException;
import java.io.StreamCorruptedException;
import java.util.ArrayList;
import java.util.List;

import org.duineth.mobile.gps.alarm.cont.SettingsHandler;
import org.duineth.mobile.gps.alarm.res.AlarmData;

import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.ItemizedOverlay;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.OverlayItem;
import com.google.android.maps.Projection;

public class LocationFinder extends MapActivity {

	private TextView myLongitude, myLatitude;
	private CheckBox mySatellite;

	private MapView myMapView;
	private MapController myMapController;

	private Button btAdd;

	private GeoPoint geoPoint;
	//
	private SettingsHandler settingsHandler;

	private void SetSatellite() {
		myMapView.setSatellite(mySatellite.isChecked());
	};

	@Override
	protected void onCreate(Bundle icicle) {
		// TODO Auto-generated method stub
		super.onCreate(icicle);

		try {
			settingsHandler = SettingsHandler.getInstance(LocationFinder.this);
		} catch (StreamCorruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		setContentView(R.layout.location_select_view);

		myMapView = (MapView) findViewById(R.id.mapview);

		myMapController = myMapView.getController();
		myMapController.setZoom(14);

		myMapView.setBuiltInZoomControls(true);

		myLongitude = (TextView) findViewById(R.id.longitude);
		myLatitude = (TextView) findViewById(R.id.latitude);
		mySatellite = (CheckBox) findViewById(R.id.satellite);
		mySatellite.setOnClickListener(mySatelliteOnClickListener);
		btAdd = (Button) findViewById(R.id.btAdd);

		btAdd.setOnClickListener(new View.OnClickListener() {

			public void onClick(View arg0) {

				Geocoder geocoder = new Geocoder(
						LocationFinder.this,
						LocationFinder.this.getResources().getConfiguration().locale);

				double latti = geoPoint.getLatitudeE6() / 1E6;
				double lon = geoPoint.getLongitudeE6() / 1E6;
				String location = "";

				try {
					List<Address> fromLocation = geocoder.getFromLocation(
							latti, lon, 1);
					if (fromLocation != null || fromLocation.size() != 0) {
						Address address = fromLocation.get(0);
						location = address.getCountryCode() + ","
								+ address.getFeatureName();

					}

				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

				AlarmData alarmData = new AlarmData(latti, lon, location);

				try {
					settingsHandler.setAlarmDatas(alarmData,
							LocationFinder.this);

					Intent intent = new Intent(LocationFinder.this,
							MainActivity.class);
					LocationFinder.this.startActivity(intent);
					LocationFinder.this.finish();

				} catch (StreamCorruptedException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				} catch (ClassNotFoundException e) {
					e.printStackTrace();
				}

			}

		});

		SetSatellite();

		double longi = getIntent().getExtras().getDouble("long");
		double lattin = getIntent().getExtras().getDouble("lati");

		int intLatitude = (int) (lattin * 1000000);
		int intLongitude = (int) (longi * 1000000);
		GeoPoint initGeoPoint = new GeoPoint(intLatitude, intLongitude);
		CenterLocation(initGeoPoint);

	}

	private void placeMarker(int markerLatitude, int markerLongitude) {
		Drawable marker = getResources().getDrawable(
				android.R.drawable.ic_menu_myplaces);
		marker.setBounds(0, 0, marker.getIntrinsicWidth(),
				marker.getIntrinsicHeight());
		myMapView.getOverlays().add(
				new InterestingLocations(marker, markerLatitude,
						markerLongitude));
	}

	@Override
	protected boolean isRouteDisplayed() {
		return false;
	}

	private void CenterLocation(GeoPoint centerGeoPoint) {
		this.geoPoint = centerGeoPoint;

		myMapController.animateTo(centerGeoPoint);

		myLongitude
				.setText("Longitude: "
						+ String.valueOf((float) centerGeoPoint
								.getLongitudeE6() / 1000000));
		myLatitude
				.setText("Latitude: "
						+ String.valueOf((float) centerGeoPoint.getLatitudeE6() / 1000000));
		placeMarker(centerGeoPoint.getLatitudeE6(),
				centerGeoPoint.getLongitudeE6());
	};

	private CheckBox.OnClickListener mySatelliteOnClickListener = new CheckBox.OnClickListener() {

		public void onClick(View v) {
			SetSatellite();
		}
	};

	class InterestingLocations extends ItemizedOverlay<OverlayItem> {

		private List<OverlayItem> locations = new ArrayList<OverlayItem>();
		private Drawable marker;
		private OverlayItem myOverlayItem;

		boolean MoveMap;

		public InterestingLocations(Drawable defaultMarker, int LatitudeE6,
				int LongitudeE6) {
			super(defaultMarker);
			this.marker = defaultMarker;
			GeoPoint myPlace = new GeoPoint(LatitudeE6, LongitudeE6);
			myOverlayItem = new OverlayItem(myPlace, "My Place", "My Place");
			locations.add(myOverlayItem);

			populate();
		}

		@Override
		protected OverlayItem createItem(int i) {
			return locations.get(i);
		}

		@Override
		public int size() {
			return locations.size();
		}

		@Override
		public void draw(Canvas canvas, MapView mapView, boolean shadow) {
			super.draw(canvas, mapView, shadow);

			boundCenterBottom(marker);
		}

		@Override
		public boolean onTouchEvent(MotionEvent arg0, MapView arg1) {

			int Action = arg0.getAction();
			if (Action == MotionEvent.ACTION_MOVE) {

				if (!MoveMap) {
					Projection proj = myMapView.getProjection();
					GeoPoint loc = proj.fromPixels((int) arg0.getX(),
							(int) arg0.getY());

					myMapView.getOverlays().remove(0);

					CenterLocation(loc);
				}

			} else if (Action == MotionEvent.ACTION_DOWN) {

				MoveMap = false;

			} else if (Action == MotionEvent.ACTION_MOVE) {
				MoveMap = true;
			}

			return super.onTouchEvent(arg0, arg1);

		}

		@Override
		public boolean onTap(GeoPoint p, MapView mapView) {

			Projection proj = myMapView.getProjection();

			myMapView.getOverlays().remove(0);

			CenterLocation(p);
			return super.onTap(p, mapView);
		}

	}

}