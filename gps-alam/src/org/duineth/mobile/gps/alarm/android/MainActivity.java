package org.duineth.mobile.gps.alarm.android;

import java.io.IOException;
import java.io.StreamCorruptedException;
import java.util.ArrayList;
import java.util.List;

import org.duineth.mobile.gps.alarm.cont.LocationListnerHandler;
import org.duineth.mobile.gps.alarm.cont.SettingsHandler;
import org.duineth.mobile.gps.alarm.res.AlarmData;
import org.duineth.mobile.gps.alarm.res.AlarmListAdapter;
import org.duineth.mobile.gps.alarm.res.Observers;

import android.app.Activity;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

public class MainActivity extends Activity implements Observers {

	List<AlarmData> alarmDatas;

	private ListView lvAlarms;

	private Button btAdd;

	private LocationListnerHandler lcHandler = LocationListnerHandler
			.getInstance();

	private Location location;

	private SettingsHandler settingsHandler;;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		lcHandler.addObservers(MainActivity.this);
		try {
			lcHandler.startLocation(MainActivity.this);
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}


		
		setContentView(R.layout.activity_main);

		btAdd = (Button) findViewById(R.id.btAdd);
		btAdd.setOnClickListener(new View.OnClickListener() {

			public void onClick(View arg0) {

				double lati = 7.5000;
				double longti = 80.06292;

				if (location != null) {
					lati = location.getLatitude();
					longti = location.getLongitude();
				}

				Intent intent = new Intent(MainActivity.this,
						LocationFinder.class);

				intent.putExtra("long", longti);
				intent.putExtra("lati", lati);

				MainActivity.this.startActivity(intent);
				MainActivity.this.finish();

			}
		});

		try {
			loadData();
		} catch (StreamCorruptedException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		
		Intent service=new Intent(MainActivity.this, AlarmService.class);
		MainActivity.this.startService(service);

	}

	private void loadData() throws StreamCorruptedException, IOException,
			ClassNotFoundException {

		settingsHandler = SettingsHandler.getInstance(MainActivity.this);

		lvAlarms = (ListView) findViewById(R.id.listView1);

		alarmDatas = new ArrayList<AlarmData>();

		alarmDatas = settingsHandler.getAlarmDatas();

		lvAlarms.setAdapter(new AlarmListAdapter(alarmDatas, MainActivity.this));

		

	}

	@Override
	protected void onResume() {
		try {
			loadData();
		} catch (StreamCorruptedException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		super.onResume();

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

	public void update(Location location) {
		this.location = location;
	}

}
