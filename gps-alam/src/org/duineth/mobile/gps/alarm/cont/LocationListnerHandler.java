package org.duineth.mobile.gps.alarm.cont;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import org.duineth.mobile.gps.alarm.res.Observers;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;

public class LocationListnerHandler  implements ObserverSubject {
	private static LocationListnerHandler handler;
	private static String TAG = LocationListnerHandler.class.getName();
	private LocationManager lm;
	private Mylocationlistener ll;
	private boolean isRunning = false;

	private Context context;

	private SettingsHandler settingsHandler;
	private Timer timer;
	private TimerTask task;

	private List<Observers> observerList = new ArrayList<Observers>();

	private LocationListnerHandler() {
		settingsHandler = SettingsHandler.getInstance();
	}


	public void addObservers(Observers observers) {

		observerList.add(observers);
	}

	public synchronized void startLocation(Context context)
			throws NumberFormatException, IOException {

		Log.i(TAG, "UPDATE");
		if (!isRunning) {

			if (lm == null) {
				lm = (LocationManager) context
						.getSystemService(Context.LOCATION_SERVICE);
			}

			if (ll == null) {
				ll = new Mylocationlistener();
			}

			this.context = context;
			lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, ll);
			timer = new Timer();
			int timetrange = 10000;
			timetrange = getTimeRange();
			task = new TimerTask() {

				@Override
				public void run() {
					for (Observers observers : observerList) {
						observers.update(ll.location);
					}
				}
			};
			timer.schedule(task, 100, timetrange);
			isRunning = true;
		}

	}

	private int getTimeRange() throws NumberFormatException, IOException {
		
		return 1;
	}

	public synchronized void stopLocation() {
		lm.removeUpdates(ll);
		timer.cancel();
		isRunning = false;
	}

	private class Mylocationlistener implements LocationListener {

		final ObserverSubject listnerHandler = LocationListnerHandler
				.getInstance();

		private Location location;

		public void onLocationChanged(Location location) {

			if (location != null) {
				this.location = location;

			}
		}

		public void onProviderDisabled(String provider) {
		}

		public void onProviderEnabled(String provider) {
		}

		public void onStatusChanged(String provider, int status, Bundle extras) {
		}
	}

	public static synchronized LocationListnerHandler getInstance() {
		if (handler == null) {
			handler = new LocationListnerHandler();
		}
		return handler;
	}

	public boolean isRunning() {
		return isRunning;
	}

	public void removeObservers(Observers observers) {
		observerList.remove(observers);
	}

	
}
