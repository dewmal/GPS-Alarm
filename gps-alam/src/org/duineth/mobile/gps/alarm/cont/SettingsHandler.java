package org.duineth.mobile.gps.alarm.cont;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.io.StreamCorruptedException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.duineth.mobile.gps.alarm.res.AlarmData;
import org.duineth.mobile.gps.alarm.res.SaveSettings;

import android.content.Context;
import android.provider.Contacts.Settings;
import android.util.Log;

public class SettingsHandler implements Serializable {

	private static SaveSettings settings;

	private static String FILENAME = "gpsalarmsettings.settings";

	private static SettingsHandler settingsHandler;

	private SettingsHandler() {

	}

	public static SettingsHandler getInstance() {
		if (settingsHandler == null) {
			settingsHandler = new SettingsHandler();
		}
		return settingsHandler;

	}

	public static SettingsHandler getInstance(Context context)
			throws StreamCorruptedException, IOException,
			ClassNotFoundException {
		if (settingsHandler == null) {
			settingsHandler = new SettingsHandler();
		}
		SettingsHandler.load(context);
		if (SettingsHandler.settings == null) {
			SettingsHandler.settings = new SaveSettings();
		}
		return settingsHandler;

	}

	private static void load(Context context) throws StreamCorruptedException,
			IOException, ClassNotFoundException {
		try {
			FileInputStream fis = context.openFileInput(FILENAME);
			ObjectInputStream is = new ObjectInputStream(fis);
			SettingsHandler.settings = (SaveSettings) is.readObject();
			is.close();
		} catch (FileNotFoundException e) {
			SettingsHandler.settings = new SaveSettings();
			save(context, SettingsHandler.settings);
			e.printStackTrace();
		}
	}

	public static void save(Context context, SaveSettings settings)
			throws IOException {
		FileOutputStream fos = context.openFileOutput(FILENAME,
				Context.MODE_PRIVATE);
		ObjectOutputStream os = new ObjectOutputStream(fos);
		os.writeObject(settings);
		os.close();
	}

	public List<AlarmData> getAlarmDatas() {

		return settings.getAlarmDatas();
	}
	
	
	public static void setAlarmdata(List<AlarmData> alarmDatas, Context context) throws StreamCorruptedException, IOException, ClassNotFoundException{
		
		SettingsHandler.settings=new SaveSettings();
		SettingsHandler.settings.setAlarmDatas(alarmDatas);
		save(context, SettingsHandler.settings);
	}
	

	public static void setAlarmDatas(AlarmData alarmData, Context context)
			throws StreamCorruptedException, IOException,
			ClassNotFoundException {
		
		
		
		if (SettingsHandler.settings == null) {
			load(context);
		}
		
		
		int size = settings.getAlarmDatas().size();
		alarmData.setId(size++);
		if (SettingsHandler.settings.getAlarmDatas() != null) {
		
			AlarmData data = null;
			for (AlarmData alaData : settings.getAlarmDatas()) {
				if (alaData.getId() == alarmData.getId()) {
					data=alaData;
					settings.getAlarmDatas().remove(alaData);
					Log.i("TAGGG",data.getId()+"");
					break;
				}
			}

			if(data!=null){
				data.setLatti(alarmData.getLatti());
				data.setLon(alarmData.getLon());
				data.setLocation(alarmData.getLocation());				
			}else{				
				data=alarmData;
				
			}
			
			Log.i("TAGGY", data.getId()+"");
			
			
			SettingsHandler.settings.getAlarmDatas().add(data);
			save(context, SettingsHandler.settings);

		}
	}

}
