package org.duineth.mobile.gps.alarm.res;

import java.io.Serializable;

import android.location.Location;

public class AlarmData implements Serializable{
	
	
	
	private int id=0;
	private  double latti;
	private  double lon;
	private  String location;
	private boolean working;
	
	public AlarmData(double latti, double lon, String location) {		
		this.latti = latti;
		this.lon = lon;
		this.location = location;
	}

	public double getDistance(Location curLc){
		return distFrom(latti, lon,curLc.getLatitude() , curLc.getLongitude());
	}
	
	public double getLatti() {
		return latti;
	}
	public void setLatti(double latti) {
		this.latti = latti;
	}
	public double getLon() {
		return lon;
	}
	public void setLon(double lon) {
		this.lon = lon;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}

	public boolean isWorking() {
		return working;
	}

	public void setWorking(boolean working) {
		this.working = working;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	
	 public double distFrom(double lat1, double lng1, double lat2, double lng2) {
         double earthRadius = 3958.75;
         double dLat = Math.toRadians(lat2 - lat1);
         double dLng = Math.toRadians(lng2 - lng1);
         double a = Math.sin(dLat / 2) * Math.sin(dLat / 2)
                         + Math.cos(Math.toRadians(lat1))
                         * Math.cos(Math.toRadians(lat2)) * Math.sin(dLng / 2)
                         * Math.sin(dLng / 2);
         double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
         double dist = earthRadius * c;

         int meterConversion = 1609;

         return (dist * meterConversion);
 }

}
