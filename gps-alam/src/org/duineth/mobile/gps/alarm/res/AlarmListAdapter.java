package org.duineth.mobile.gps.alarm.res;

import java.io.IOException;
import java.io.StreamCorruptedException;
import java.util.List;

import org.duineth.mobile.gps.alarm.android.R;
import org.duineth.mobile.gps.alarm.cont.SettingsHandler;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

public class AlarmListAdapter extends BaseAdapter {
	private LayoutInflater mInflater;

	private List<AlarmData> alarmDataList;

	private Context context;

	private SettingsHandler settingsHandler;

	public AlarmListAdapter(List<AlarmData> alarmDataList, Context context)
			throws StreamCorruptedException, IOException,
			ClassNotFoundException {
		super();
		settingsHandler = SettingsHandler.getInstance(context);
		this.alarmDataList = alarmDataList;
		this.context = context;
		this.mInflater = LayoutInflater.from(context);
	}

	public int getCount() {
		return alarmDataList.size();
	}

	public Object getItem(int arg0) {
		return arg0;
	}

	public long getItemId(int arg0) {
		return arg0;
	}

	public View getView(final int position, View convertView, ViewGroup parent) {
		final ViewHolder holder;
		
		Log.i("TAG", position+"");
		
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.alrm_list_view, null);
			holder = new ViewHolder();
			holder.tvlcName = (TextView) convertView.findViewById(R.id.tvName);
			holder.tvlong = (TextView) convertView
					.findViewById(R.id.tvLongitude);

			holder.tvlati = (TextView) convertView
					.findViewById(R.id.tvLatitude);

			holder.tbOnOff = (CheckBox) convertView
					.findViewById(R.id.btAlarmOn);
			holder.tbOnOff
					.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

						public void onCheckedChanged(CompoundButton arg0,
								boolean arg1) {
							AlarmData alarmData2 = alarmDataList
									.remove(position);
							alarmData2.setWorking(holder.tbOnOff.isChecked());
							alarmDataList.add(alarmData2);
							try {
								settingsHandler.setAlarmDatas(alarmData2,
										context);
							} catch (StreamCorruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (ClassNotFoundException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					});

			convertView.setTag(holder);
		} else {

			holder = (ViewHolder) convertView.getTag();

		}
		AlarmData alarmData = alarmDataList.get(position);
		holder.tvlcName.setText(alarmData.getLocation());
		holder.tvlati.setText(alarmData.getLatti() + "");
		holder.tvlong.setText(alarmData.getLon() + "");
		holder.tbOnOff.setChecked(alarmData.isWorking());

		Log.i("TAG", "");

		convertView.setOnLongClickListener(new View.OnLongClickListener() {

			public boolean onLongClick(View arg0) {
				alarmDataList.remove(position);

				try {
					settingsHandler.setAlarmdata(alarmDataList, context);
				} catch (StreamCorruptedException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				} catch (ClassNotFoundException e) {
					e.printStackTrace();
				}

				AlarmListAdapter.this.notifyDataSetChanged();

				Log.i("ALARM ADAPTER", "Cilcked " + position);
				return false;
			}

		});

		return convertView;
	}

	static class ViewHolder {
		TextView tvlcName;
		TextView tvlong;
		TextView tvlati;
		CheckBox tbOnOff;

	}
}
