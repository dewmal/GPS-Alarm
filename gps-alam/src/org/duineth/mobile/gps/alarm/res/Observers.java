package org.duineth.mobile.gps.alarm.res;

import android.location.Location;

public interface Observers {

	void update(Location location);

	
	
}
