package org.duineth.mobile.gps.alarm.res;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class SaveSettings implements Serializable{
	private  List<AlarmData> alarmDatas;
	
	
	public SaveSettings(){
		alarmDatas=new ArrayList<AlarmData>();
	}

	public List<AlarmData> getAlarmDatas() {
		return alarmDatas;
	}

	public void setAlarmDatas(List<AlarmData> alarmDatas) {
		this.alarmDatas = alarmDatas;
	}
}
